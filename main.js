import { app, BrowserWindow } from 'electron'

const createWindow = () => {
    // Windows size
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    })

    // load index.html
    win.loadFile('./src/index.html')
}

app.whenReady().then(createWindow)

console.log(
    app.getPath('userData'),
    app.getPath('cache'),
)
